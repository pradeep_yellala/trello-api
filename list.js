const createList = (name, id) => {
  const listContainer = document.querySelector(".list-container");
  const list = document.createElement("div");
  list.classList.add("list");
  list.id = id;
  list.innerHTML = `<h3>${name}</h3>
                     <div class = "deletelistIcon">
                       <i class="fa fa-trash"></i>
                     </div>
                     <button type="button" class="addCard">
                         <i class="fa fa-plus plus"></i>
                         <p>Add a Card...</p>
                     </button>`;

  let addListDiv = document.querySelector(".add-list");
  listContainer.insertBefore(list, addListDiv);
};

export default createList;

function createItem(name, Id, checkListId) {
  const checkItemBoxes = document.getElementById(checkListId);
  const checkItemDiv = document.createElement("div");
  checkItemDiv.classList.add("form-check", "checkBox");
  checkItemDiv.id = Id;
  checkItemDiv.innerHTML = ` <input type="checkbox" class="form-check-input" id="exampleCheck1">
                               <label class="form-check-label" for="exampleCheck1">${name}</label>`;
  checkItemBoxes.append(checkItemDiv);
}

export default createItem;

function createCheckList(name, Id) {
  const checkListContainer = document.querySelector(
    ".cardDescriptionCheckLists"
  );
  const checkListDiv = document.createElement("div");
  checkListDiv.classList.add = ".checklist";
  checkListDiv.id = Id;
  checkListDiv.innerHTML = `<div class="checklistName">
                                        <i class="fa fa-check-square"></i>
                                        <span class="checklistname">${name}</span>
                                        <span class = "delete">Delete</span>
                                    </div>
                                    <div class="checkBoxes">
                                    </div>
                                    <div class="addItem">
                                        <i class="fa fa-plus plus"></i>
                                        <p>Add an Item...</p>
                                    </div>`;

  checkListContainer.append(checkListDiv);
}

export default createCheckList;

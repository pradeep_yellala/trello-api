const createCard = (name, id, listId) => {
  const list = document.getElementById(listId);
  const card = document.createElement("div");
  card.classList.add("cardName");
  card.id = id;
  card.innerHTML = `<p>${name}</p>`;
  const cardDetails = document.createElement("div");
  cardDetails.innerHTML = `<div class="cardDetails hide"><div class="align hide"><i class="fa fa-align-justify"></i></div>
  <div class="checkListDetails hide"><i class="far fa-check-square"></i><div class="score">1/3</div></div></div>`;
  card.append(cardDetails);
  const addCardDiv = list.querySelector(".addCard");
  list.insertBefore(card, addCardDiv);
};

export default createCard;

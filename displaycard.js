function displayCard(name, id, desc) {
  const cardBackground = document.createElement("div");
  cardBackground.classList.add("blackBackground");
  cardBackground.innerHTML = ` <div id=${id} class="container-fluid cardDiscription">
                                    <div class="cardDiscriptionDiv1">
                                        <div class="cardDiscriptionName">
                                            <i class="fa fa-folder "></i>
                                            <span class="cardnamedisplay">${name}</span>
                                        </div>
                                        <div class="cardDiscriptionDetails">                                    
                                            <i class="fa fa-align-justify"></i>                                                                          
                                            <span class="description">Description</span>                                                                          
                                            <span class="add">Add</span>                                          
                                        </div>
                                        <div class="descriptionBox">${desc}</div>
                                       
                                        <div class="cardDescriptionCheckLists">

                                        </div>
                                    </div>
                                    <div class="cardDiscriptionDiv2">
                                        <i class="fa fa-times cancel"></i>
                                        <aside>
                                            <div class="checklist">Check list</div>
                                            <div>Move</div>
                                            <div class="update">Update</div>
                                            <div class="deleteCard">Delete</div>
                                        </aside>
                                    </div>
                                </div>`;
  const container = document.querySelector(".sections-container");
  container.append(cardBackground);
}

export default displayCard;

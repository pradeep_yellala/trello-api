const popup = (title) => {
  const form = document.createElement("form");

  const formDiv1 = document.createElement("div");
  formDiv1.classList.add("form-group", "formDiv1");

  const formDiv2 = document.createElement("div");
  formDiv2.classList.add("formDiv2");

  formDiv1.innerHTML = `<label for="exampleInputEmail1">${title} : </label>
    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter ${title} here...">`;
  formDiv2.innerHTML = `<button type="submit" class="btn btn-outline-success button">Save</button>
    <i class="fa fa-times"></i>`;

  form.append(formDiv1);
  form.append(formDiv2);

  const popupDiv = document.createElement("div");
  popupDiv.classList.add("popup");
  popupDiv.append(form);
  document.body.append(popupDiv);
};

export default popup;

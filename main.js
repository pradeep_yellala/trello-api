import popup from "./popup.js";

const createBoardBtn = document.getElementById("btn");
const createBoardPlus = document.getElementById("nav-plus");
const parentDiv = document.getElementById("boards");

const token =
  "db7e333c3e9cdd0d0567671439ddbb23f9f051f8b1aa7ee85ec9dfb0746be7a0";
const key = "618b5d40317cafd7d1813a8e89116b13";

/* Rendering all the boards in the server */

window.addEventListener("load", async function () {
  const promise = await fetch(
    `https://api.trello.com/1/members/me/boards/?key=${key}&token=${token}`
  );
  const boardArr = await promise.json();

  boardArr.forEach((board) => {
    createBoard(board.name, board.id);
  });
});

/* create board in UI */

function createBoard(title, id) {
  const board = document.createElement("div");
  const aTag = document.createElement("a");
  board.classList.add("boards");
  board.id = id;
  aTag.setAttribute("href", `board.html?id=${id}&name=${title}`);
  aTag.style.display = "flex";
  aTag.style.justifyContent = "center";
  aTag.style.padding = "10px";
  const titleDiv = document.createElement("h1");

  aTag.append(titleDiv);
  titleDiv.style.fontSize = "1em";
  titleDiv.style.color = "#f4f4f4";
  titleDiv.innerText = title;
  board.append(aTag);
  parentDiv.insertBefore(board, parentDiv.lastElementChild);
}

createBoardBtn.addEventListener("click", createBoardServer);
createBoardPlus.addEventListener("click", createBoardServer);

function createBoardServer() {
  popup("Board Name");
  const text = document.querySelector(".popup input");
  const save = document.querySelector(".popup button");
  const cancel = document.querySelector(".popup i");
  const popupDiv = document.querySelector(".popup");
  cancel.addEventListener("click", () => {
    popupDiv.remove();
  });
  save.addEventListener("click", async function (e) {
    e.preventDefault();
    const promise = await fetch(
      `https://api.trello.com/1/boards/?key=${key}&token=${token}&name=${text.value}`,
      {
        method: "POST",
      }
    );
    const data = await promise.json();
    createBoard(data.name, data.id);
    popupDiv.remove();
  });
}
